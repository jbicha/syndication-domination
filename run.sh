#!/bin/bash

set -e

if [[ ! -d build ]]; then
    mkdir build
    pushd build
    meson .. -Dprefix="$PWD/mprefix" -DTO_JSON_BINARY=true -DPYTHON_BINDINGS=true -DHTML_SUPPORT=true
    popd
fi

ninja -C build
ninja -C build install
./build/mprefix/bin/SyndicationDomination "$@"
